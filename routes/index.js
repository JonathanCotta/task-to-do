const express = require("express");
const router  = express.Router();

const taskCtrl = require("../controllers/taskController");
const userCtlr = require("../controllers/userController");

const auth = require("../middleware/auth");

router.get("/task/", auth(), taskCtrl.getAll);
router.get("/task/create/", auth(), taskCtrl.create);
router.post("/task/create/", auth(), taskCtrl.createForm);
router.get("/task/edit/:id", auth(), taskCtrl.edit);
router.post("/task/edit/:id", auth(), taskCtrl.editForm);
router.delete("/task/:id", auth(), taskCtrl.delete);


router.get("/users", auth(), userCtlr.getAll);
router.get("/user/:id", auth(), userCtlr.getOne);
router.post("/user/", auth(), userCtlr.create);
router.put("/user/", auth(), userCtlr.update);
router.delete("/user/:id", auth(), userCtlr.delete);

router.post("/login", userCtlr.login);
router.get("/logout", userCtlr.logout);

module.exports = router;