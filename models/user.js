const mongoose   = require('../database');
const bcrypt     = require("bcrypt");
const Schema     = mongoose.Schema;
const saltRounds = 10;

let userSchema = new Schema({
    name: {
        type: String, 
        required: true
    },
    password: {
        type: String, 
        required: true
    },
    admin: {
        type: Boolean, 
        required: true
    }
});

userSchema.statics.genHash = function (pwd) {
    return bcrypt.hashSync(pwd, saltRounds);
}

userSchema.statics.validHash = function (data, hash) {
    return bcrypt.compareSync(data, hash);
}

module.exports = mongoose.model('User', userSchema);