const mongoose = require("../database");
const schema   = mongoose.Schema;

let taskSchema = new schema({
    text: {
        type: String,
        required: true
    } ,
    deadline: {
        type: Date,
        required: true
    },
    date: {
        type: Date, 
        default: Date.now
    }
});

module.exports = mongoose.model("task", taskSchema);