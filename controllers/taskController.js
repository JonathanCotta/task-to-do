const Task = require("../models/task");

exports.getAll = (req, res) => {
    Task.find({}).then((tasks) =>{
        res.render("index", { tasks: tasks });
    });   
};

exports.create = (req, res) => {

    let task = {
        text:"",
        deadline: ""
    }

    res.render("taskForm", { action: "create", title: "Create", task: {text: "fazer arroz", deadline:"21/07/2200"} });
};

exports.createForm = (req, res) => {
    Task.create(req.body).then(() => res.redirect("/task/"));
};

exports.edit = (req, res) => {
    Task.findById({ _id: req.params.id }).then((task) => {
        res.render("taskForm", { task: task, title: "Edit" });
    });
};

exports.editForm = (req, res) => {
    Task.updateOne({ _id: req.body.id }, req.body).then(() => res.redirect("/task/"));
};

exports.delete = (req, res) => {
    Task.deleteOne({ _id: req.params.id }).then(() => res.redirect("/task/"));
};