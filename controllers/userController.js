const User = require("../models/user");

exports.getAll = (req, res) => {
    User.find({})
        .then(users => res.json(users))
        .catch(err => res.json({ status: "fail", response: err.messasge }));
}

exports.getOne = (req, res) => {
    User.findOne({ _id: req.params.id })
        .then(user => res.json(user))
        .catch(err => res.json({ status: "fail", response: err.message }));
}

exports.create = (req, res) => {

    User.create({
        name: req.body.name,
        password: User.genHash(req.body.password),
        admin: req.body.admin
    })
        .then(user => res.send(`User ${user.name} was successfully created`))
        .catch(err => res.json({ status: "fail", response: err.message }));
}

exports.update = (req, res) => {

    User.update({ _id: req.body.id },
        {
            name: req.body.name,
            password: User.genHash(req.body.password),
            admin: req.body.admin
        })
        .then(raw => res.json({ status: "success", response: raw }))
        .catch(err => res.json({ status: "fail", response: err.message }));
}

exports.delete = (req, res) => {
    User.deleteOne({ _id: req.params.id })
        .then(() => res.json({ status: "success", response: "user successfully removed" }))
        .catch(err => res.json({ status: "fail", response: err.message }));
}

exports.login = (req, res) => {

    User.findOne({ name: req.body.name }, (err, user) => {

        if (err)
            return res.json({ success: false, err: err.message });

        if (!user)
            return res.json({ success: false, message: 'Authentication failed. User not found.' });

        if (!User.validHash(req.body.password, user.password))
            return res.json({ success: false, message: `Authentication failed. Wrong password.` });

        req.session.isLoged = true;
        req.session.admin = user.admin;

        res.json({ success: true, session: req.session });
    });
}

exports.logout = (req, res) => {
    req.session.destroy((err) => {
        if (err)
            return res.send({ status: "fail", response: err.message });

        res.json({ success: true, message: `Session finished` });
    });
}

exports.sessionInfo = (req, res) => {
    res.json({ ssid: req.session.id, sessionData: req.session });
}