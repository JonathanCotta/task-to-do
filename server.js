const express    = require("express");
const morgan     = require("morgan");
const session    = require("express-session");
const mongoStore = require("connect-mongodb-session")(session);

const router = require("./routes/index");
const config = require("./config");
const db     = require("./database/").connection;

const app   = express();
const port  = process.env.PORT || 3000;
const store = new mongoStore({uri: db.connection, collection: 'sessions'});

app.set("view engine", "pug");
app.set("views","./views");

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use(session({
    secret: config.secret,
    resave: false,
    saveUninitialized: true,
    cookie: { 
        secure: false,
        maxAge: 3600*1000
    },
    store: store
}));

app.use(morgan("dev"));

app.use("/", router);

app.listen(port, () => console.log(`running at localhost:${port}/task/  press ctrl + c to stop \n`));